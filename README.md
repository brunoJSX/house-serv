# Requisitos

* Ter o node pré-instalado;
* Ter o docker e docker-compose pré instalado;
* Ter o gerenciador de pacotes yarn pré instalado;

# Instalar dependências

Na pasta raiz do projeto dê o comando:

```
$ yarn
```

Este comando irá instalar todos os pacotes necessários para que a aplicação funcione


# Subir container do POSTGRES


Na pasta raiz do projeto dê o comando:

> Note: Antes de rodar o comando deve se verificar se não existe
> nenhum serviço rodando na porta 5432, caso exista altere o docker-compose.yml
> para subir o container em outra porta;

```
$ docker-compose up -d
```

Depois do container subido, crie uma banco de dados chamado house_service


# Rodando migrations

Na pasta raiz do projeto dê o comando:

```
$ yarn typeorm migration:run
```

Isso irá criar todas tabelas necessárias e popular algumas tabelas.


# Colocando o server pra rodar ✨🎉

Na pasta raiz do projeto dê o comando:

```
$ yarn dev:server
```

Pronto seu server já está em funcionamento ✨.
