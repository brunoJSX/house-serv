import ICreateCityDTO from '@modules/users/dtos/ICreateCityDTO';
import City from '@modules/users/infra/typeorm/entities/City';

export default interface ICitiesRepository {
  findById(id: number): Promise<City | undefined>;
  create(data: ICreateCityDTO): Promise<City>;
}
