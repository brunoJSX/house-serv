import UserContactType from '@modules/users/infra/typeorm/entities/UserContactType';

export default interface IUserContactTypesRepository {
  findByName(name: string): Promise<UserContactType | undefined>;
  create(name: string): Promise<UserContactType>;
  deleteByName(name: string): Promise<void>;
}
