import ICreateUserContactDTO from '@modules/users/dtos/ICreateUserContactDTO';

import UserContact from '@modules/users/infra/typeorm/entities/UserContact';

export default interface IUserContactsRepository {
  findByTypeAndValue(
    type: string,
    value: string,
  ): Promise<UserContact | undefined>;
  create(data: ICreateUserContactDTO): Promise<UserContact>;
}
