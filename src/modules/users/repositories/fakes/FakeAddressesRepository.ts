import { uuid } from 'uuidv4';

import IFindAddressDTO from '@modules/users/dtos/IFindAddressDTO';
import ICreateAddressDTO from '@modules/users/dtos/ICreateAddressDTO';
import IAddressesRepository from '@modules/users/repositories/IAddressesRepository';

import Address from '@modules/users/infra/typeorm/entities/Address';

class FakeAddressesRepository implements IAddressesRepository {
  private addresses: Address[] = [];

  public async find({
    city_id,
    street,
    neighborhood,
    number,
  }: IFindAddressDTO): Promise<Address | undefined> {
    const address = this.addresses.find(
      findAddress =>
        findAddress.city_id === city_id &&
        findAddress.street === street &&
        findAddress.neighborhood === neighborhood &&
        findAddress.number === number,
    );

    return address;
  }

  public async create(data: ICreateAddressDTO): Promise<Address> {
    const address = new Address();

    Object.assign(address, {
      id: uuid(),
      ...data,
      created_at: new Date(),
      updated_at: new Date(),
    });

    this.addresses.push(address);

    return address;
  }
}

export default FakeAddressesRepository;
