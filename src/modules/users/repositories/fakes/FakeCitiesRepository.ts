import { uuid } from 'uuidv4';

import ICreateCityDTO from '@modules/users/dtos/ICreateCityDTO';
import ICitiesRepository from '@modules/users/repositories/ICitiesRepository';

import City from '@modules/users/infra/typeorm/entities/City';

class FakeCitiesRepository implements ICitiesRepository {
  private cities: City[] = [];

  public async findById(id: number): Promise<City | undefined> {
    const city = this.cities.find(findCity => findCity.id === id);

    return city;
  }

  public async create(data: ICreateCityDTO): Promise<City> {
    const city = new City();

    Object.assign(city, {
      id: uuid(),
      ...data,
      created_at: new Date(),
      updated_at: new Date(),
    });

    this.cities.push(city);

    return city;
  }
}

export default FakeCitiesRepository;
