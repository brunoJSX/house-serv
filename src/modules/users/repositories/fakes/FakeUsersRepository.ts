import { uuid } from 'uuidv4';

import ICreateUserDTO from '@modules/users/dtos/ICreateUserDTO';
import IUsersRepository from '@modules/users/repositories/IUsersRepository';
import FakeUserContactsRepository from '@modules/users/repositories/fakes/FakeUserContactsRepository';
import FakeAddressesRepository from '@modules/users/repositories/fakes/FakeAddressesRepository';

import User from '@modules/users/infra/typeorm/entities/User';
import UserContact from '@modules/users/infra/typeorm/entities/UserContact';

class FakeUsersRepository implements IUsersRepository {
  private users: User[] = [];

  constructor(
    private fakeUserContactsRepository: FakeUserContactsRepository,
    private fakeAddressesRepository: FakeAddressesRepository,
  ) {}

  public async create({
    contacts,
    ...userData
  }: ICreateUserDTO): Promise<User> {
    const user = new User();

    Object.assign(user, { id: uuid() }, userData);

    contacts.map(contact => {
      return this.fakeUserContactsRepository.create({
        ...contact,
        user_id: user.id,
        user,
      });
    });

    user.contacts = contacts as UserContact[];
    user.addresses = [];

    this.users.push(user);

    return user;
  }

  public async findById(id: string): Promise<User | undefined> {
    const user = this.users.find(findUser => findUser.id === id);

    return user;
  }

  public async save(user: User): Promise<User> {
    const findIndex = this.users.findIndex(findUser => findUser.id === user.id);

    const updatedAddresses = user.addresses.map(address =>
      this.fakeAddressesRepository.create(address),
    );

    user.addresses = await Promise.all(updatedAddresses);

    this.users[findIndex] = user;

    return user;
  }
}

export default FakeUsersRepository;
