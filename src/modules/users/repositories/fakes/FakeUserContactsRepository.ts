import { uuid } from 'uuidv4';

import ICreateUserContactDTO from '@modules/users/dtos/ICreateUserContactDTO';
import IUserContactsRepository from '@modules/users/repositories/IUserContactsRepository';

import UserContact from '@modules/users/infra/typeorm/entities/UserContact';

class FakeUserContactsRepository implements IUserContactsRepository {
  private contacts: UserContact[] = [];

  public async findByTypeAndValue(
    type: string,
    value: string,
  ): Promise<UserContact | undefined> {
    const contact = this.contacts.find(
      findContact =>
        findContact.contact_type_id === type && findContact.value === value,
    );

    return contact;
  }

  public async create(data: ICreateUserContactDTO): Promise<UserContact> {
    const contact = new UserContact();

    Object.assign(contact, { id: uuid(), ...data });

    this.contacts.push(contact);

    return contact;
  }
}

export default FakeUserContactsRepository;
