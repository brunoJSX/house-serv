import { uuid } from 'uuidv4';

import IUserContactTypesRepository from '@modules/users/repositories/IUserContactTypesRepository';

import UserContactType from '@modules/users/infra/typeorm/entities/UserContactType';

class FakeUserContactTypesRepository implements IUserContactTypesRepository {
  private contactTypes: UserContactType[] = [];

  constructor() {
    this.contactTypes.push({
      id: uuid(),
      name: 'email',
      status: true,
      created_at: new Date(),
      updated_at: new Date(),
    });
  }

  public async findByName(name: string): Promise<UserContactType | undefined> {
    const contactType = this.contactTypes.find(
      findContactType => findContactType.name === name,
    );

    return contactType;
  }

  public async create(name: string): Promise<UserContactType> {
    const contactType = new UserContactType();

    Object.assign(contactType, { id: uuid(), name });

    this.contactTypes.push(contactType);

    return contactType;
  }

  public async deleteByName(name: string): Promise<void> {
    const findIndex = this.contactTypes.findIndex(
      findContactType => findContactType.name === name,
    );

    this.contactTypes.splice(findIndex, 1);
  }
}

export default FakeUserContactTypesRepository;
