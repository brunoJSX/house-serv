import IFindAddressDTO from '@modules/users/dtos/IFindAddressDTO';
import ICreateAddressDTO from '@modules/users/dtos/ICreateAddressDTO';
import Address from '@modules/users/infra/typeorm/entities/Address';

export default interface IAddressesRepository {
  find(data: IFindAddressDTO): Promise<Address | undefined>;
  create(data: ICreateAddressDTO): Promise<Address>;
}
