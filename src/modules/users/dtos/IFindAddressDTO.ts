export default interface IFindAddressDTO {
  city_id: number;
  street: string;
  neighborhood: string;
  number: string;
}
