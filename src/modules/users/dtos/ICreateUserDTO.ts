import ICreateUserContactDTO from './ICreateUserContactDTO';

export default interface ICreateUserDTO {
  name: string;
  password: string;
  contacts: ICreateUserContactDTO[];
}
