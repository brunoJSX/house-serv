export default interface ICreateAddressDTO {
  city_id: number;
  street: string;
  neighborhood: string;
  complement?: string;
  number: string;
  zip_code: number;
}
