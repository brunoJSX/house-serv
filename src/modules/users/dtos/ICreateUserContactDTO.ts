import User from '@modules/users/infra/typeorm/entities/User';

export default interface ICreateUserContactDTO {
  contact_type_id: string;
  user_id?: string;
  value: string;
  user?: User;
}
