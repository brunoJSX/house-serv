export default interface ICreateCityDTO {
  name: string;
  state_id: number;
}
