import { getRepository, Repository } from 'typeorm';

import ICreateUserContactDTO from '@modules/users/dtos/ICreateUserContactDTO';
import IUserContactsRepository from '@modules/users/repositories/IUserContactsRepository';

import UserContact from '@modules/users/infra/typeorm/entities/UserContact';

class UserContactsRepository implements IUserContactsRepository {
  ormRepository: Repository<UserContact>;

  constructor() {
    this.ormRepository = getRepository(UserContact);
  }

  public async findByTypeAndValue(
    type: string,
    value: string,
  ): Promise<UserContact | undefined> {
    const contact = await this.ormRepository.findOne({
      where: { contact_type_id: type, value },
      relations: ['user'],
    });

    return contact;
  }

  public async create(data: ICreateUserContactDTO): Promise<UserContact> {
    const contact = this.ormRepository.create(data);

    this.ormRepository.save(contact);

    return contact;
  }
}

export default UserContactsRepository;
