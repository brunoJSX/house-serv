import { getRepository, Repository } from 'typeorm';

import ICreateCityDTO from '@modules/users/dtos/ICreateCityDTO';
import ICitiesRepository from '@modules/users/repositories/ICitiesRepository';

import City from '@modules/users/infra/typeorm/entities/City';

class CitiesRepository implements ICitiesRepository {
  ormRepository: Repository<City>;

  constructor() {
    this.ormRepository = getRepository(City);
  }

  public async findById(id: number): Promise<City | undefined> {
    const city = await this.ormRepository.findOne({ where: { id } });

    return city;
  }

  public async create(data: ICreateCityDTO): Promise<City> {
    const city = this.ormRepository.create(data);

    this.ormRepository.save(city);

    return city;
  }
}

export default CitiesRepository;
