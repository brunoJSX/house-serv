import { getRepository, Repository } from 'typeorm';

import IUserContactTypesRepository from '@modules/users/repositories/IUserContactTypesRepository';

import UserContactType from '@modules/users/infra/typeorm/entities/UserContactType';

class UserContactTypesRepository implements IUserContactTypesRepository {
  ormRepository: Repository<UserContactType>;

  constructor() {
    this.ormRepository = getRepository(UserContactType);
  }

  public async findByName(name: string): Promise<UserContactType | undefined> {
    const contactType = await this.ormRepository.findOne({
      where: { name },
    });

    return contactType;
  }

  public async create(name: string): Promise<UserContactType> {
    const contactType = this.ormRepository.create({ name });

    this.ormRepository.save(contactType);

    return contactType;
  }

  public async deleteByName(name: string): Promise<void> {
    await this.ormRepository.delete({ name });
  }
}

export default UserContactTypesRepository;
