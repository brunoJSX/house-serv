import { getRepository, Repository } from 'typeorm';

import IFindAddressDTO from '@modules/users/dtos/IFindAddressDTO';
import ICreateAddressDTO from '@modules/users/dtos/ICreateAddressDTO';
import IAddressesRepository from '@modules/users/repositories/IAddressesRepository';

import Address from '@modules/users/infra/typeorm/entities/Address';

class AddressesRepository implements IAddressesRepository {
  ormRepository: Repository<Address>;

  constructor() {
    this.ormRepository = getRepository(Address);
  }

  public async find({
    city_id,
    street,
    neighborhood,
    number,
  }: IFindAddressDTO): Promise<Address | undefined> {
    const address = await this.ormRepository.findOne({
      where: { city_id, street, neighborhood, number },
    });

    return address;
  }

  public async create(data: ICreateAddressDTO): Promise<Address> {
    const address = this.ormRepository.create(data);

    this.ormRepository.save(data);

    return address;
  }
}

export default AddressesRepository;
