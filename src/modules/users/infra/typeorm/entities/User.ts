import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
  JoinTable,
  ManyToMany,
} from 'typeorm';

import UserContact from './UserContact';
import Address from './Address';

@Entity('users')
class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column()
  password: string;

  @OneToMany(() => UserContact, contact => contact.user, {
    cascade: ['insert'],
    eager: true,
  })
  @JoinTable()
  contacts: UserContact[];

  @ManyToMany(() => Address, () => User, {
    cascade: ['insert'],
    eager: true,
  })
  @JoinTable({
    name: 'relations_users_addresses',
    joinColumns: [{ name: 'user_id' }],
    inverseJoinColumns: [{ name: 'address_id' }],
  })
  addresses: Address[];

  @Column()
  status: boolean;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}

export default User;
