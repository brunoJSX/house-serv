import { Router } from 'express';
import { celebrate, Segments, Joi } from 'celebrate';

import ensureAuthenticated from '../middlewares/ensureAuthenticated';

import UsersController from '../controllers/UsersController';
import UserAddressesController from '../controllers/UserAddressesController';

const usersRouter = Router();

const usersController = new UsersController();
const userAddressesController = new UserAddressesController();

usersRouter.post(
  '/',
  celebrate({
    [Segments.BODY]: {
      name: Joi.string().required(),
      password: Joi.string().min(6).required(),
      email: Joi.string().email().required(),
    },
  }),
  usersController.create,
);

usersRouter.use(ensureAuthenticated);

usersRouter.post(
  '/addresses',
  celebrate({
    [Segments.BODY]: {
      city_id: Joi.number().strict().required(),
      street: Joi.string().required(),
      neighborhood: Joi.string().required(),
      complement: Joi.string(),
      number: Joi.string().required(),
      zip_code: Joi.number().strict().required(),
    },
  }),
  userAddressesController.create,
);

export default usersRouter;
