import { Request, Response } from 'express';
import { container } from 'tsyringe';

import AddUserAddressService from '@modules/users/services/AddUserAddressService';

class UserAddressesController {
  public async create(request: Request, response: Response): Promise<Response> {
    const { id: user_id } = request.user;
    const {
      city_id,
      street,
      neighborhood,
      complement,
      number,
      zip_code,
    } = request.body;

    const addUserAddress = container.resolve(AddUserAddressService);

    const user = await addUserAddress.execute({
      user_id,
      addressData: {
        city_id,
        street,
        neighborhood,
        complement,
        number,
        zip_code,
      },
    });

    delete user.password;

    return response.json(user);
  }
}

export default UserAddressesController;
