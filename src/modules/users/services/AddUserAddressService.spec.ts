import AppError from '@shared/errors/AppError';

import FakeHashProvider from '@modules/users/providers/HashProvider/fakes/FakeHashProvider';

import FakeUsersRepository from '@modules/users/repositories/fakes/FakeUsersRepository';
import FakeUserContactsRepository from '@modules/users/repositories/fakes/FakeUserContactsRepository';
import FakeUserContactTypesRepository from '@modules/users/repositories/fakes/FakeUserContactTypesRepository';
import FakeAddressesRepository from '@modules/users/repositories/fakes/FakeAddressesRepository';
import FakeCitiesRepository from '@modules/users/repositories/fakes/FakeCitiesRepository';

import CreateUserService from '@modules/users/services/CreateUserService';
import AddUserAddressService from '@modules/users/services/AddUserAddressService';

let fakeUsersRepository: FakeUsersRepository;
let fakeUserContactTypesRepository: FakeUserContactTypesRepository;
let fakeUserContactsRepository: FakeUserContactsRepository;
let fakeAddressesRepository: FakeAddressesRepository;
let fakeCitiesRepository: FakeCitiesRepository;

let fakeHashProvider: FakeHashProvider;

let createUser: CreateUserService;
let addUserAddress: AddUserAddressService;

describe('AddUserAddress', () => {
  beforeEach(() => {
    fakeUserContactsRepository = new FakeUserContactsRepository();
    fakeAddressesRepository = new FakeAddressesRepository();
    fakeUsersRepository = new FakeUsersRepository(
      fakeUserContactsRepository,
      fakeAddressesRepository,
    );
    fakeUserContactTypesRepository = new FakeUserContactTypesRepository();
    fakeCitiesRepository = new FakeCitiesRepository();

    fakeHashProvider = new FakeHashProvider();

    createUser = new CreateUserService(
      fakeUsersRepository,
      fakeUserContactTypesRepository,
      fakeUserContactsRepository,
      fakeHashProvider,
    );

    addUserAddress = new AddUserAddressService(
      fakeUsersRepository,
      fakeAddressesRepository,
      fakeCitiesRepository,
    );
  });

  it('should be able to add address.', async () => {
    const { id: city_id } = await fakeCitiesRepository.create({
      name: 'Ji-Paraná',
      state_id: 1,
    });

    const { id: user_id } = await createUser.execute({
      name: 'John Doe',
      password: '123456',
      email: 'johndoe@gmail.com',
    });

    const addressData = {
      zip_code: 12345678,
      city_id,
      street: 'Rua de teste',
      neighborhood: 'Bairro de teste',
      complement: 'casa',
      number: '1234',
    };

    const userUpdated = await addUserAddress.execute({
      user_id,
      addressData,
    });

    expect(userUpdated.addresses).toEqual(
      expect.arrayContaining([expect.objectContaining(addressData)]),
    );
  });

  it('should not be able to add address for non-existing user.', async () => {
    const addressData = {
      zip_code: 12345678,
      city_id: 1,
      street: 'Rua de teste',
      neighborhood: 'Bairro de teste',
      complement: 'casa',
      number: '1234',
    };

    await expect(
      addUserAddress.execute({
        user_id: 'non-existing-user',
        addressData,
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should not be able to add address with non-existing city.', async () => {
    const { id: user_id } = await createUser.execute({
      name: 'John Doe',
      password: '123456',
      email: 'johndoe@gmail.com',
    });

    const addressData = {
      zip_code: 12345678,
      city_id: 1,
      street: 'Rua de teste',
      neighborhood: 'Bairro de teste',
      complement: 'casa',
      number: '1234',
    };

    await expect(
      addUserAddress.execute({ user_id, addressData }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should not be able to add address if already registered.', async () => {
    const { id: city_id } = await fakeCitiesRepository.create({
      name: 'Ji-Paraná',
      state_id: 1,
    });

    const { id: user_id } = await createUser.execute({
      name: 'John Doe',
      password: '123456',
      email: 'johndoe@gmail.com',
    });

    const addressData = {
      zip_code: 12345678,
      city_id,
      street: 'Rua de teste',
      neighborhood: 'Bairro de teste',
      complement: 'casa',
      number: '1234',
    };

    await addUserAddress.execute({ user_id, addressData });

    await expect(
      addUserAddress.execute({ user_id, addressData }),
    ).rejects.toBeInstanceOf(AppError);
  });
});
