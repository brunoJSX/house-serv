import { injectable, inject } from 'tsyringe';

import AppError from '@shared/errors/AppError';

import IHashProvider from '@modules/users/providers/HashProvider/models/IHashProvider';

import IUsersRepository from '@modules/users/repositories/IUsersRepository';
import IUserContactsRepository from '@modules/users/repositories/IUserContactsRepository';
import IUserContactTypesRepository from '@modules/users/repositories/IUserContactTypesRepository';

import User from '@modules/users/infra/typeorm/entities/User';

interface IRequest {
  name: string;
  password: string;
  email: string;
}

@injectable()
class CreateUserService {
  constructor(
    @inject('UsersRepository')
    private usersRepository: IUsersRepository,

    @inject('UserContactTypesRepository')
    private userContactTypesRepository: IUserContactTypesRepository,

    @inject('UserContactsRepository')
    private userContactsRepository: IUserContactsRepository,

    @inject('HashProvider')
    private hashProvider: IHashProvider,
  ) {}

  public async execute({ name, password, email }: IRequest): Promise<User> {
    const contactType = await this.userContactTypesRepository.findByName(
      'email',
    );

    if (!contactType) {
      throw new AppError('Contact type does not exists.');
    }

    const emailAlreadyExists = await this.userContactsRepository.findByTypeAndValue(
      contactType.id,
      email,
    );

    if (emailAlreadyExists) {
      throw new AppError('Email already exists.');
    }

    const passwordHashed = await this.hashProvider.generateHash(password);

    const user = await this.usersRepository.create({
      name,
      password: passwordHashed,
      contacts: [
        {
          contact_type_id: contactType.id,
          value: email,
        },
      ],
    });

    return user;
  }
}

export default CreateUserService;
