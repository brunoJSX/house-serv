import AppError from '@shared/errors/AppError';

import FakeHashProvider from '@modules/users/providers/HashProvider/fakes/FakeHashProvider';

import FakeUsersRepository from '@modules/users/repositories/fakes/FakeUsersRepository';
import FakeUserContactsRepository from '@modules/users/repositories/fakes/FakeUserContactsRepository';
import FakeUserContactTypesRepository from '@modules/users/repositories/fakes/FakeUserContactTypesRepository';
import FakeAddressesRepository from '@modules/users/repositories/fakes/FakeAddressesRepository';
import CreateUserService from '@modules/users/services/CreateUserService';

let fakeUsersRepository: FakeUsersRepository;
let fakeUserContactTypesRepository: FakeUserContactTypesRepository;
let fakeUserContactsRepository: FakeUserContactsRepository;
let fakeAddressesRepository: FakeAddressesRepository;
let fakeHashProvider: FakeHashProvider;
let createUser: CreateUserService;

describe('CreateUser', () => {
  beforeEach(() => {
    fakeUserContactsRepository = new FakeUserContactsRepository();
    fakeAddressesRepository = new FakeAddressesRepository();
    fakeUsersRepository = new FakeUsersRepository(
      fakeUserContactsRepository,
      fakeAddressesRepository,
    );
    fakeUserContactTypesRepository = new FakeUserContactTypesRepository();
    fakeHashProvider = new FakeHashProvider();

    createUser = new CreateUserService(
      fakeUsersRepository,
      fakeUserContactTypesRepository,
      fakeUserContactsRepository,
      fakeHashProvider,
    );
  });

  it('should be able to create new user.', async () => {
    const user = await createUser.execute({
      name: 'John Doe',
      password: '123456',
      email: 'bruno@cbkdigital.com.br',
    });

    expect(user).toHaveProperty('id');
  });

  it('should not be able to create new user with contact type EMAIL non-existing', async () => {
    fakeUserContactTypesRepository.deleteByName('email');

    await expect(
      createUser.execute({
        name: 'John Doe',
        password: '123456',
        email: 'bruno@cbkdigital.com.br',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should not be able to create new user with an email address already in use', async () => {
    await createUser.execute({
      name: 'John Doe',
      password: '123456',
      email: 'bruno@cbkdigital.com.br',
    });

    await expect(
      createUser.execute({
        name: 'John Doe',
        password: '123456',
        email: 'bruno@cbkdigital.com.br',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should not be able to create new user without encrypting password', async () => {
    const generateHash = jest.spyOn(fakeHashProvider, 'generateHash');

    const user = await createUser.execute({
      name: 'John Doe',
      password: '123456',
      email: 'bruno@cbkdigital.com.br',
    });

    expect(generateHash).toBeCalledWith('123456');
    expect(user).toHaveProperty('id');
  });
});
