import { injectable, inject } from 'tsyringe';
import { sign } from 'jsonwebtoken';

import AppError from '@shared/errors/AppError';
import authConfig from '@config/auth';

import IHashProvider from '@modules/users/providers/HashProvider/models/IHashProvider';

import IUserContactsRepository from '@modules/users/repositories/IUserContactsRepository';
import IUserContactTypesRepository from '@modules/users/repositories/IUserContactTypesRepository';

import User from '@modules/users/infra/typeorm/entities/User';

interface IRequest {
  email: string;
  password: string;
}

interface IResponse {
  token: string;
  user: User;
}

@injectable()
class AuthenticateUserService {
  constructor(
    @inject('UserContactsRepository')
    private userContactsRepository: IUserContactsRepository,

    @inject('UserContactTypesRepository')
    private userContactTypesRepository: IUserContactTypesRepository,

    @inject('HashProvider')
    private hashProvider: IHashProvider,
  ) {}

  public async execute({ email, password }: IRequest): Promise<IResponse> {
    const contactType = await this.userContactTypesRepository.findByName(
      'email',
    );

    if (!contactType) {
      throw new AppError('Contact type EMAIL does not exists.');
    }

    const contact = await this.userContactsRepository.findByTypeAndValue(
      contactType.id,
      email,
    );

    if (!contact) {
      throw new AppError('Incorrect email/password combination');
    }

    const passwordMatched = await this.hashProvider.compareHash(
      password,
      contact.user.password,
    );

    if (!passwordMatched) {
      throw new AppError('Incorrect email/password combination');
    }

    const { secret, expiresIn } = authConfig.jwt;

    const token = sign({}, secret, {
      subject: contact.user_id,
      expiresIn,
    });

    return {
      user: contact.user,
      token,
    };
  }
}

export default AuthenticateUserService;
