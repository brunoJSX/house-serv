import { injectable, inject } from 'tsyringe';
import path from 'path';

import AppError from '@shared/errors/AppError';

import IMailProvider from '@shared/container/providers/MailProvider/models/IMailProvider';

import IUserContactTypesRepository from '@modules/users/repositories/IUserContactTypesRepository';
import IUserContactsRepository from '@modules/users/repositories/IUserContactsRepository';
import IUserTokensRepository from '@modules/users/repositories/IUserTokensRepository';

interface IRequest {
  email: string;
}

@injectable()
class SendForgotPasswordEmailService {
  constructor(
    @inject('MailProvider')
    private mailProvider: IMailProvider,

    @inject('UserContactTypesRepository')
    private userContactTypesRepository: IUserContactTypesRepository,

    @inject('UserContactsRepository')
    private userContactsRepository: IUserContactsRepository,

    @inject('UserTokensRepository')
    private userTokensRepository: IUserTokensRepository,
  ) {}

  public async execute({ email }: IRequest): Promise<void> {
    const contactType = await this.userContactTypesRepository.findByName(
      'email',
    );

    if (!contactType) {
      throw new AppError('Contact type does not exists');
    }

    const contact = await this.userContactsRepository.findByTypeAndValue(
      contactType.id,
      email,
    );

    if (!contact) {
      throw new AppError('This email address does not exists');
    }

    const { user } = contact;

    const { token } = await this.userTokensRepository.generate(user.id);

    const forgotPasswordTemplate = path.resolve(
      __dirname,
      '..',
      'views',
      'forgot_password.hbs',
    );

    await this.mailProvider.sendMail({
      to: {
        name: user.name,
        email: contact.value,
      },
      subject: '[House Service] Recuperação de senha',
      templateData: {
        file: forgotPasswordTemplate,
        variables: {
          name: user.name,
          link: `http://localhost:3000/reset_password?token=${token}`,
        },
      },
    });
  }
}

export default SendForgotPasswordEmailService;
