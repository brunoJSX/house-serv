import AppError from '@shared/errors/AppError';

import FakeUsersRepository from '@modules/users/repositories/fakes/FakeUsersRepository';
import FakeUserContactsRepository from '@modules/users/repositories/fakes/FakeUserContactsRepository';
import FakeUserContactTypesRepository from '@modules/users/repositories/fakes/FakeUserContactTypesRepository';
import FakeHashProvider from '@modules/users/providers/HashProvider/fakes/FakeHashProvider';
import FakeAddressesRepository from '@modules/users/repositories/fakes/FakeAddressesRepository';

import CreateUserService from '@modules/users/services/CreateUserService';
import AuthenticateUserService from '@modules/users/services/AuthenticateUserService';

let fakeUsersRepository: FakeUsersRepository;
let fakeUserContactsRepository: FakeUserContactsRepository;
let fakeUserContactTypesRepository: FakeUserContactTypesRepository;
let fakeAddressesRepository: FakeAddressesRepository;

let fakeHashProvider: FakeHashProvider;

let authenticateUser: AuthenticateUserService;
let createUser: CreateUserService;

describe('AuthenticateUser', () => {
  beforeEach(() => {
    fakeUserContactsRepository = new FakeUserContactsRepository();
    fakeAddressesRepository = new FakeAddressesRepository();
    fakeUsersRepository = new FakeUsersRepository(
      fakeUserContactsRepository,
      fakeAddressesRepository,
    );
    fakeUserContactTypesRepository = new FakeUserContactTypesRepository();
    fakeHashProvider = new FakeHashProvider();

    authenticateUser = new AuthenticateUserService(
      fakeUserContactsRepository,
      fakeUserContactTypesRepository,
      fakeHashProvider,
    );

    createUser = new CreateUserService(
      fakeUsersRepository,
      fakeUserContactTypesRepository,
      fakeUserContactsRepository,
      fakeHashProvider,
    );
  });

  it('should be able to authenticate', async () => {
    await createUser.execute({
      name: 'John Doe',
      password: '123456',
      email: 'johndoe@example.com',
    });

    const response = await authenticateUser.execute({
      email: 'johndoe@example.com',
      password: '123456',
    });

    expect(response).toHaveProperty('token');
  });

  it('should not be able to authenticate with contact type EMAIL non-existing', async () => {
    fakeUserContactTypesRepository.deleteByName('email');

    await expect(
      authenticateUser.execute({
        email: 'johndoe@example.com',
        password: '123456',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should not be able to authenticate with non-existing contact/user', async () => {
    await expect(
      authenticateUser.execute({
        email: 'johndoe@example.com',
        password: '123456',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should not be able to authenticate with wrong password', async () => {
    await createUser.execute({
      name: 'John Doe',
      password: '123456',
      email: 'johndoe@example.com',
    });

    await expect(
      authenticateUser.execute({
        email: 'johndoe@example.com',
        password: 'wrong-password',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });
});
