import AppError from '@shared/errors/AppError';

import FakeHashProvider from '@modules/users/providers/HashProvider/fakes/FakeHashProvider';
import FakeMailProvider from '@shared/container/providers/MailProvider/fakes/FakeMailProvider';

import FakeUsersRepository from '@modules/users/repositories/fakes/FakeUsersRepository';
import FakeUserContactsRepository from '@modules/users/repositories/fakes/FakeUserContactsRepository';
import FakeUserContactTypesRepository from '@modules/users/repositories/fakes/FakeUserContactTypesRepository';
import FakeUserTokensRepository from '@modules/users/repositories/fakes/FakeUserTokensRepository';
import FakeAddressesRepository from '@modules/users/repositories/fakes/FakeAddressesRepository';

import CreateUserService from '@modules/users/services/CreateUserService';
import SendForgotPasswordEmailService from '@modules/users/services/SendForgotPasswordEmailService';

let usersRepository: FakeUsersRepository;
let fakeUserContactTypesRepository: FakeUserContactTypesRepository;
let fakeUserContactsRepository: FakeUserContactsRepository;
let fakeUserTokensRepository: FakeUserTokensRepository;
let fakeAddressesRepository: FakeAddressesRepository;

let fakeHashProvider: FakeHashProvider;
let fakeMailProvider: FakeMailProvider;

let createUser: CreateUserService;
let sendForgotPasswordEmail: SendForgotPasswordEmailService;

describe('CreateUser', () => {
  beforeEach(() => {
    fakeUserContactsRepository = new FakeUserContactsRepository();
    fakeAddressesRepository = new FakeAddressesRepository();
    usersRepository = new FakeUsersRepository(
      fakeUserContactsRepository,
      fakeAddressesRepository,
    );
    fakeUserContactTypesRepository = new FakeUserContactTypesRepository();
    fakeHashProvider = new FakeHashProvider();
    fakeMailProvider = new FakeMailProvider();
    fakeUserTokensRepository = new FakeUserTokensRepository();

    createUser = new CreateUserService(
      usersRepository,
      fakeUserContactTypesRepository,
      fakeUserContactsRepository,
      fakeHashProvider,
    );

    sendForgotPasswordEmail = new SendForgotPasswordEmailService(
      fakeMailProvider,
      fakeUserContactTypesRepository,
      fakeUserContactsRepository,
      fakeUserTokensRepository,
    );
  });

  it('should be able to recover email for password reset', async () => {
    const sendEmail = jest.spyOn(fakeMailProvider, 'sendMail');

    await createUser.execute({
      name: 'John Doe',
      password: '123456',
      email: 'bruno@cbkdigital.com.br',
    });

    await sendForgotPasswordEmail.execute({
      email: 'bruno@cbkdigital.com.br',
    });

    expect(sendEmail).toHaveBeenCalled();
  });

  it('should not be able to recover with contact type EMAIL non-existing', async () => {
    fakeUserContactTypesRepository.deleteByName('email');

    await expect(
      sendForgotPasswordEmail.execute({
        email: 'bruno@cbkdigital.com.br',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should not be able to recover with non-existing contact/user', async () => {
    await expect(
      sendForgotPasswordEmail.execute({
        email: 'johndoe@example.com',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should generate a forgot password token', async () => {
    const generate = jest.spyOn(fakeUserTokensRepository, 'generate');

    const user = await createUser.execute({
      name: 'John Doe',
      password: '123456',
      email: 'bruno@cbkdigital.com.br',
    });

    await sendForgotPasswordEmail.execute({
      email: 'bruno@cbkdigital.com.br',
    });

    expect(generate).toHaveBeenCalledWith(user.id);
  });
});
