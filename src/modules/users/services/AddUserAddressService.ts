import { injectable, inject } from 'tsyringe';

import AppError from '@shared/errors/AppError';

import ICreateAddressDTO from '@modules/users/dtos/ICreateAddressDTO';

import IUsersRepository from '@modules/users/repositories/IUsersRepository';
import IAddressesRepository from '@modules/users/repositories/IAddressesRepository';
import ICitiesRepository from '@modules/users/repositories/ICitiesRepository';

import User from '@modules/users/infra/typeorm/entities/User';
import Address from '@modules/users/infra/typeorm/entities/Address';

interface IRequest {
  user_id: string;
  addressData: ICreateAddressDTO;
}

@injectable()
class AddUserAddressService {
  constructor(
    @inject('UsersRepository')
    private usersRepository: IUsersRepository,

    @inject('AddressesRepository')
    private addressesRepository: IAddressesRepository,

    @inject('CitiesRepository')
    private citiesRepository: ICitiesRepository,
  ) {}

  public async execute({ user_id, addressData }: IRequest): Promise<User> {
    const user = await this.usersRepository.findById(user_id);

    if (!user) {
      throw new AppError('User does not exists');
    }

    const city = await this.citiesRepository.findById(addressData.city_id);

    if (!city) {
      throw new AppError('City does not exists');
    }

    const findedAddress = await this.addressesRepository.find(addressData);

    const checkUserAlreadyHasAddress = user.addresses.find(
      addressFinded => addressFinded.id === findedAddress?.id,
    );

    if (checkUserAlreadyHasAddress) {
      throw new AppError('You already have this address registered.');
    }

    user.addresses.push(findedAddress || (addressData as Address));

    return this.usersRepository.save(user);
  }
}

export default AddUserAddressService;
