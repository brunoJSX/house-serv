import AppError from '@shared/errors/AppError';

import FakeHashProvider from '@modules/users/providers/HashProvider/fakes/FakeHashProvider';

import FakeUsersRepository from '@modules/users/repositories/fakes/FakeUsersRepository';
import FakeUserContactsRepository from '@modules/users/repositories/fakes/FakeUserContactsRepository';
import FakeUserContactTypesRepository from '@modules/users/repositories/fakes/FakeUserContactTypesRepository';
import FakeUserTokensRepository from '@modules/users/repositories/fakes/FakeUserTokensRepository';
import FakeAddressesRepository from '@modules/users/repositories/fakes/FakeAddressesRepository';

import CreateUserService from '@modules/users/services/CreateUserService';
import ResetPasswordService from '@modules/users/services/ResetPasswordService';

let fakeusersRepository: FakeUsersRepository;
let fakeUserContactTypesRepository: FakeUserContactTypesRepository;
let fakeUserContactsRepository: FakeUserContactsRepository;
let fakeUserTokensRepository: FakeUserTokensRepository;
let fakeAddressesRepository: FakeAddressesRepository;

let fakeHashProvider: FakeHashProvider;

let createUser: CreateUserService;
let resetPassword: ResetPasswordService;

describe('ResetPassword', () => {
  beforeEach(() => {
    fakeUserContactsRepository = new FakeUserContactsRepository();
    fakeAddressesRepository = new FakeAddressesRepository();
    fakeusersRepository = new FakeUsersRepository(
      fakeUserContactsRepository,
      fakeAddressesRepository,
    );
    fakeUserContactTypesRepository = new FakeUserContactTypesRepository();
    fakeHashProvider = new FakeHashProvider();
    fakeUserTokensRepository = new FakeUserTokensRepository();

    createUser = new CreateUserService(
      fakeusersRepository,
      fakeUserContactTypesRepository,
      fakeUserContactsRepository,
      fakeHashProvider,
    );

    resetPassword = new ResetPasswordService(
      fakeusersRepository,
      fakeUserTokensRepository,
      fakeHashProvider,
    );
  });

  it('should be able to reset password', async () => {
    const user = await createUser.execute({
      name: 'John Doe',
      password: '123456',
      email: 'bruno@cbkdigital.com.br',
    });

    const { token } = await fakeUserTokensRepository.generate(user.id);

    const generateHash = jest.spyOn(fakeHashProvider, 'generateHash');

    await resetPassword.execute({
      token,
      password: '1234567',
    });

    const updatedUser = await fakeusersRepository.findById(user.id);

    expect(generateHash).toHaveBeenCalledWith('1234567');
    expect(updatedUser?.password).toBe('1234567');
  });

  it('should not be able to reset password with non-existing token', async () => {
    expect(
      resetPassword.execute({
        token: 'non-existing-token',
        password: '1234567',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should not be able to reset password with non-existing user', async () => {
    const { token } = await fakeUserTokensRepository.generate(
      'non-existing-user',
    );

    expect(
      resetPassword.execute({
        token,
        password: '1234567',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should not be able to reset password if passed more than 2 hours', async () => {
    const user = await createUser.execute({
      name: 'John Doe',
      password: '123456',
      email: 'bruno@cbkdigital.com.br',
    });

    const { token } = await fakeUserTokensRepository.generate(user.id);

    jest.spyOn(Date, 'now').mockImplementationOnce(() => {
      const customDate = new Date();

      return customDate.setHours(customDate.getHours() + 3);
    });

    await expect(
      resetPassword.execute({
        token,
        password: '1234567',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });
});
