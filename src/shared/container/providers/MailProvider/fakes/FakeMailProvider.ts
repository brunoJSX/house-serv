import ISendEmailDTO from '../dtos/ISendEmailDTO';
import IMailProvider from '../models/IMailProvider';

class FakeMailProvider implements IMailProvider {
  messages: ISendEmailDTO[] = [];

  public async sendMail(data: ISendEmailDTO): Promise<void> {
    this.messages.push(data);
  }
}

export default FakeMailProvider;
