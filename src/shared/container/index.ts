import { container } from 'tsyringe';

import '@modules/users/providers';
import './providers';

import IUsersRepository from '@modules/users/repositories/IUsersRepository';
import UsersRepository from '@modules/users/infra/typeorm/repositories/UsersRepository';

import IUserContactTypesRepository from '@modules/users/repositories/IUserContactTypesRepository';
import UserContactTypesRepository from '@modules/users/infra/typeorm/repositories/UserContactTypesRepository';

import IUserContactsRepository from '@modules/users/repositories/IUserContactsRepository';
import UserContactsRepository from '@modules/users/infra/typeorm/repositories/UserContactsRepository';

import IUserTokensRepository from '@modules/users/repositories/IUserTokensRepository';
import UserTokensRepository from '@modules/users/infra/typeorm/repositories/UserTokensRepository';

import ICitiesRepository from '@modules/users/repositories/ICitiesRepository';
import CitiesRepository from '@modules/users/infra/typeorm/repositories/CitiesRepository';

import IAddressesRepository from '@modules/users/repositories/IAddressesRepository';
import AddressesRepository from '@modules/users/infra/typeorm/repositories/AddressesRepository';

container.registerSingleton<IUsersRepository>(
  'UsersRepository',
  UsersRepository,
);

container.registerSingleton<IUserContactTypesRepository>(
  'UserContactTypesRepository',
  UserContactTypesRepository,
);

container.registerSingleton<IUserContactsRepository>(
  'UserContactsRepository',
  UserContactsRepository,
);

container.registerSingleton<IUserTokensRepository>(
  'UserTokensRepository',
  UserTokensRepository,
);

container.registerSingleton<ICitiesRepository>(
  'CitiesRepository',
  CitiesRepository,
);

container.registerSingleton<IAddressesRepository>(
  'AddressesRepository',
  AddressesRepository,
);
