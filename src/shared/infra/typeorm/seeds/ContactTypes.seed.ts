import { Seeder, Factory } from 'typeorm-seeding';
import { Connection } from 'typeorm';

const contactTypes = [
  { name: 'email' },
  { name: 'whatsapp' },
  { name: 'telefone' },
  { name: 'celular' },
];

export default class ContactTypes implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<void> {
    await connection
      .createQueryBuilder()
      .insert()
      .into('user_contact_types')
      .values(contactTypes)
      .execute();
  }
}
